<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jugadores".
 *
 * @property int $codigo_jugador
 * @property string $nombre
 * @property string|null $apellidos
 * @property int|null $dorsal
 * @property string|null $fecha_nac
 * @property string|null $posicion
 *
 * @property Forman[] $forman
 * @property Equipos[] $codigoEquipos
 * @property Goles[] $goles
 * @property Goles[] $goles0
 * @property Juegan[] $juegan
 * @property Partidos[] $codigoPartidos
 * @property Nacionalidades[] $nacionalidades
 */
class Jugadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jugadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['dorsal'], 'integer'],
            [['fecha_nac'], 'safe'],
            [['nombre'], 'string', 'max' => 30],
            [['apellidos'], 'string', 'max' => 50],
            [['posicion'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_jugador' => 'Codigo del jugador',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellido',
            'dorsal' => 'Dorsal',
            'fecha_nac' => 'Fecha de nacimiento',
            'posicion' => 'Posicion',
        ];
    }

    /**
     * Gets query for [[Forman]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getForman()
    {
        return $this->hasMany(Forman::className(), ['codigo_jugador' => 'codigo_jugador']);
    }

    /**
     * Gets query for [[CodigoEquipos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEquipos()
    {
        return $this->hasMany(Equipos::className(), ['codigo_equipo' => 'codigo_equipo'])->viaTable('forman', ['codigo_jugador' => 'codigo_jugador']);
    }

    /**
     * Gets query for [[Goles]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGoles()
    {
        return $this->hasMany(Goles::className(), ['codigo_jugador_asistencia' => 'codigo_jugador']);
    }

    /**
     * Gets query for [[Goles0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGoles0()
    {
        return $this->hasMany(Goles::className(), ['codigo_jugador_gol' => 'codigo_jugador']);
    }

    /**
     * Gets query for [[Juegan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJuegan()
    {
        return $this->hasMany(Juegan::className(), ['codigo_jugador' => 'codigo_jugador']);
    }

    /**
     * Gets query for [[CodigoPartidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPartidos()
    {
        return $this->hasMany(Partidos::className(), ['codigo_partido' => 'codigo_partido'])->viaTable('juegan', ['codigo_jugador' => 'codigo_jugador']);
    }

    /**
     * Gets query for [[Nacionalidades]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNacionalidades()
    {
        return $this->hasMany(Nacionalidades::className(), ['codigo_jugador' => 'codigo_jugador']);
    }
    
        public function afterFind() {
        parent::afterFind();
        if ($this->fecha_nac != null){ $this->fecha_nac=Yii::$app->formatter->asDate($this->fecha_nac, 'php:d-m-Y');}
        
        
    }
   
    
    public function beforeSave($insert) {
          parent::beforeSave($insert);
          //$this->fecha_publicacion=Yii::$app->formatter->asDate($this->fecha_publicacion, 'php:Y-m-d');
          if ($this->fecha_nac !=null){$this->fecha_nac= \DateTime::createFromFormat("d/m/Y", $this->fecha_nac)->format("Y/m/d");}
          return true;
    }
    
public static function dropdown(){
        static $dropdown;
        
        if($dropdown === null){
            $models = static::find()->all();
            foreach ($models as $model){
                $dropdown[$model->codigo_jugador] = $model->nombre.' '.$model->apellidos;
            }
            
        }
        return $dropdown;
    }

}
