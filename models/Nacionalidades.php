<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nacionalidades".
 *
 * @property int $codigo_nacionalidad
 * @property int|null $codigo_jugador
 * @property string|null $nombre_nacionalidad
 *
 * @property Jugadores $codigoJugador
 */
class Nacionalidades extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nacionalidades';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_nacionalidad'], 'required'],
            [['codigo_jugador'], 'integer'],
            [['nombre_nacionalidad'], 'string', 'max' => 30],
            [['codigo_jugador', 'nombre_nacionalidad'], 'unique', 'targetAttribute' => ['codigo_jugador', 'nombre_nacionalidad']],
            [['codigo_jugador'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::className(), 'targetAttribute' => ['codigo_jugador' => 'codigo_jugador']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_nacionalidad' => 'Codigo de la nacionalidad',
            'codigo_jugador' => 'Jugador',
            'nombre_nacionalidad' => 'Nacion',
        ];
    }

    /**
     * Gets query for [[CodigoJugador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoJugador()
    {
        return $this->hasOne(Jugadores::className(), ['codigo_jugador' => 'codigo_jugador']);
    }
}
