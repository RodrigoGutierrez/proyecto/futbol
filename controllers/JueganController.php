<?php

namespace app\controllers;

use Yii;
use app\models\Juegan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Partidos;
use app\models\Jugadores;
use app\models\Forman;

/**
 * JueganController implements the CRUD actions for Juegan model.
 */
class JueganController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Juegan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Juegan::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Juegan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Juegan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Juegan();
        $model_jugadores = Jugadores::find()->all();
        $model_partidos = Yii::$app->db->createCommand('SELECT partidos_fuera.codigo_partido, jornada, partidos_fuera.nombre_equipo_casa, e.nombre_equipo AS nombre_equipo_fuera
   FROM 
  (SELECT partidos1.codigo_partido, partidos1.codigo_equipo_casa,e.nombre_equipo
    nombre_equipo_casa, partidos1.codigo_equipo_fuera,e.nombre_equipo,jornada FROM 
  (SELECT p.codigo_partido, p.codigo_equipo_casa, p.codigo_equipo_fuera,p.jornada FROM partidos p) 
    partidos1 INNER JOIN equipos e ON partidos1.codigo_equipo_casa=e.codigo_equipo) 
partidos_fuera INNER JOIN equipos e ON partidos_fuera.codigo_equipo_fuera=e.codigo_equipo ORDER BY partidos_fuera.codigo_partido')->queryAll();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_juegan]);
        }

        return $this->render('create', [
            'model' => $model,
            'model_jugadores' => $model_jugadores,
            'model_partidos' => $model_partidos,
        ]);
    
    }

    /**
     * Updates an existing Juegan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model_jugadores = Jugadores::find()->all();
        $model_partidos = Yii::$app->db->createCommand('SELECT partidos_fuera.codigo_partido, jornada, partidos_fuera.nombre_equipo_casa, e.nombre_equipo AS nombre_equipo_fuera
   FROM 
  (SELECT partidos1.codigo_partido, partidos1.codigo_equipo_casa,e.nombre_equipo
    nombre_equipo_casa, partidos1.codigo_equipo_fuera,e.nombre_equipo,jornada FROM 
  (SELECT p.codigo_partido, p.codigo_equipo_casa, p.codigo_equipo_fuera,p.jornada FROM partidos p) 
    partidos1 INNER JOIN equipos e ON partidos1.codigo_equipo_casa=e.codigo_equipo) 
partidos_fuera INNER JOIN equipos e ON partidos_fuera.codigo_equipo_fuera=e.codigo_equipo ORDER BY partidos_fuera.codigo_partido')->queryAll();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_juegan]);
        }

        return $this->render('update', [
            'model' => $model,
            'model_jugadores' => $model_jugadores,
            'model_partidos' => $model_partidos,
        ]);
    }

    /**
     * Deletes an existing Juegan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Juegan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Juegan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Juegan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('La página solicitada no existe.');
    }
    
 public function actionListjugadores($id)
    {
           $countjugadores = Yii::$app->db->createCommand("SELECT COUNT(*) FROM (
  SELECT equipos.codigo_equipo_casa codigo_equipo, codigo_jugador FROM (
  SELECT codigo_equipo_casa,codigo_equipo_fuera,fecha FROM partidos
  WHERE codigo_partido=$id
  )equipos JOIN forman ON equipos.codigo_equipo_casa = forman.codigo_equipo
  WHERE (equipos.fecha>fecha_alta AND (fecha<fecha_baja OR fecha_baja IS NULL))
UNION 
SELECT equipos.codigo_equipo_fuera codigo_equipo, codigo_jugador FROM (
  SELECT codigo_equipo_casa,codigo_equipo_fuera,fecha FROM partidos
  WHERE codigo_partido=$id
  )equipos JOIN forman ON equipos.codigo_equipo_fuera = forman.codigo_equipo
  WHERE (equipos.fecha>fecha_alta AND (fecha<fecha_baja OR fecha_baja IS NULL)) 
  )cuenta")->queryAll();
           
           $jugadores = Yii::$app->db->createCommand("SELECT c1.codigo_jugador,jugadores.nombre, jugadores.apellidos FROM (
  SELECT equipos.codigo_equipo_casa codigo_equipo, codigo_jugador FROM (
  SELECT codigo_equipo_casa,codigo_equipo_fuera,fecha FROM partidos
  WHERE codigo_partido=$id
  )equipos JOIN forman ON equipos.codigo_equipo_casa = forman.codigo_equipo
  WHERE (equipos.fecha>fecha_alta AND (fecha<fecha_baja OR fecha_baja IS NULL))
UNION 
SELECT equipos.codigo_equipo_fuera codigo_equipo, codigo_jugador FROM (
  SELECT codigo_equipo_casa,codigo_equipo_fuera,fecha FROM partidos
  WHERE codigo_partido=$id
  )equipos JOIN forman ON equipos.codigo_equipo_fuera = forman.codigo_equipo
  WHERE (equipos.fecha>fecha_alta AND (fecha<fecha_baja OR fecha_baja IS NULL))
  )c1 JOIN jugadores ON c1.codigo_jugador = jugadores.codigo_jugador")->queryAll();
           
           
           
           if($countjugadores>0){
               foreach($jugadores as $result) echo "<option value='".$result['codigo_jugador']."'>".$result['nombre'].' '.$result['apellidos']."</option>";
           }
           
           else{
               echo "<option>-</option>";
           }
           
    }
    
}
