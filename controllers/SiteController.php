<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
use app\models\Juegan;
use app\models\Jugadores;
use app\models\Equipos;
use app\models\Forman;
use app\models\Partidos;
use app\models\Goles;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
                  
        return $this->render('index');
    } else {
        
        return $this->render('index_admin');
    }
    }
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
        public function actionClasificacion(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(*) FROM (SELECT e.nombre_equipo as EQUIPOS,Clasificacion.* FROM 
  
    (SELECT * FROM 
    (SELECT codigo_equipo, puntos_partidos_empatados_casa.PPEC,puntos_partidos_empatados_fuera.PPEF,puntos_partidos_ganados_casa.PPGC,puntos_partidos_ganados_fuera.PPGF,puntos_partidos_empatados_casa.PPEC+puntos_partidos_empatados_fuera.PPEF+puntos_partidos_ganados_casa.PPGC+puntos_partidos_ganados_fuera.PPGF AS PT FROM 
    (SELECT p.codigo_equipo_casa AS codigo_equipo, COUNT(*)*3 AS PPGC FROM partidos p  WHERE p.codigo_competicion=1 AND p.resultado_equipo_casa>p.resultado_equipo_fuera GROUP BY codigo_equipo) AS puntos_partidos_ganados_casa
    JOIN (SELECT p.codigo_equipo_casa AS codigo_equipo, COUNT(*) AS PPEC FROM partidos p WHERE p.codigo_competicion=1 AND p.resultado_equipo_casa=p.resultado_equipo_fuera GROUP BY codigo_equipo) AS puntos_partidos_empatados_casa
    USING(codigo_equipo) 
    JOIN (SELECT p.codigo_equipo_fuera AS codigo_equipo, COUNT(*)*3 AS PPGF FROM partidos p WHERE p.codigo_competicion=1 AND p.resultado_equipo_fuera>p.resultado_equipo_casa GROUP BY codigo_equipo) AS puntos_partidos_ganados_fuera
    USING(codigo_equipo)
    JOIN (
      SELECT p.codigo_equipo_fuera AS codigo_equipo, COUNT(*) AS PPEF FROM partidos p WHERE p.codigo_competicion=1 AND p.resultado_equipo_fuera=p.resultado_equipo_casa GROUP BY codigo_equipo
      UNION 
      SELECT p.codigo_equipo_fuera AS codigo_equipo, 0 AS PPEF FROM partidos p WHERE p.codigo_competicion=1 AND p.codigo_equipo_fuera = 10
      ) AS puntos_partidos_empatados_fuera
    USING(codigo_equipo) GROUP BY codigo_equipo ORDER BY PT DESC) AS Puntos
  JOIN (SELECT codigo_equipo,GFC.GFC,GFF.GFF,GCC.GCC,GCF.GCF,(GFC.GFC+GFF.GFF)-(GCC.GCC+GCF.GCF) AS DIF FROM
    (SELECT p.codigo_equipo_casa AS codigo_equipo, SUM(p.resultado_equipo_casa) AS GFC FROM partidos p WHERE p.codigo_competicion=1 GROUP BY p.codigo_equipo_casa) AS GFC
    JOIN (SELECT p.codigo_equipo_fuera AS codigo_equipo, SUM(p.resultado_equipo_fuera) AS GFF FROM partidos p WHERE p.codigo_competicion=1 GROUP BY p.codigo_equipo_fuera) AS GFF
    USING (codigo_equipo)
    JOIN (SELECT p.codigo_equipo_casa AS codigo_equipo, SUM(p.resultado_equipo_fuera) AS GCC FROM partidos p WHERE p.codigo_competicion=1 GROUP BY p.codigo_equipo_casa) AS GCC
    USING (codigo_equipo)
    JOIN (SELECT p.codigo_equipo_fuera AS codigo_equipo, SUM(p.resultado_equipo_casa) AS GCF FROM partidos p WHERE p.codigo_competicion=1 GROUP BY p.codigo_equipo_fuera) AS GCF
    USING (codigo_equipo) GROUP BY codigo_equipo ORDER BY DIF DESC) AS Goles USING (codigo_equipo)
    
    JOIN (SELECT partidos_casa.codigo_equipo, partidos_casa.num_partidos_casa+partidos_fuera.num_partidos_fuera PJ FROM (
    SELECT codigo_equipo_casa codigo_equipo, COUNT(*) num_partidos_casa FROM partidos
    WHERE codigo_competicion=1
    GROUP BY codigo_equipo_casa)partidos_casa INNER JOIN (
    SELECT codigo_equipo_fuera codigo_equipo, COUNT(*) num_partidos_fuera FROM partidos
  WHERE codigo_competicion=1
  GROUP BY codigo_equipo_fuera) partidos_fuera USING (codigo_equipo) GROUP BY codigo_equipo) AS PJ USING (codigo_equipo)) AS Clasificacion JOIN equipos e USING (codigo_equipo) ORDER BY Clasificacion.PT DESC) AS clasi')->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT e.nombre_equipo as EQUIPOS,Clasificacion.* FROM 
  
    (SELECT * FROM 
    (SELECT codigo_equipo, puntos_partidos_empatados_casa.PPEC,puntos_partidos_empatados_fuera.PPEF,puntos_partidos_ganados_casa.PPGC,puntos_partidos_ganados_fuera.PPGF,puntos_partidos_empatados_casa.PPEC+puntos_partidos_empatados_fuera.PPEF+puntos_partidos_ganados_casa.PPGC+puntos_partidos_ganados_fuera.PPGF AS PT FROM 
    (SELECT p.codigo_equipo_casa AS codigo_equipo, COUNT(*)*3 AS PPGC FROM partidos p  WHERE p.codigo_competicion=1 AND p.resultado_equipo_casa>p.resultado_equipo_fuera GROUP BY codigo_equipo) AS puntos_partidos_ganados_casa
    JOIN (SELECT p.codigo_equipo_casa AS codigo_equipo, COUNT(*) AS PPEC FROM partidos p WHERE p.codigo_competicion=1 AND p.resultado_equipo_casa=p.resultado_equipo_fuera GROUP BY codigo_equipo) AS puntos_partidos_empatados_casa
    USING(codigo_equipo) 
    JOIN (SELECT p.codigo_equipo_fuera AS codigo_equipo, COUNT(*)*3 AS PPGF FROM partidos p WHERE p.codigo_competicion=1 AND p.resultado_equipo_fuera>p.resultado_equipo_casa GROUP BY codigo_equipo) AS puntos_partidos_ganados_fuera
    USING(codigo_equipo)
    JOIN (
      SELECT p.codigo_equipo_fuera AS codigo_equipo, COUNT(*) AS PPEF FROM partidos p WHERE p.codigo_competicion=1 AND p.resultado_equipo_fuera=p.resultado_equipo_casa GROUP BY codigo_equipo
      UNION 
      SELECT p.codigo_equipo_fuera AS codigo_equipo, 0 AS PPEF FROM partidos p WHERE p.codigo_competicion=1 AND p.codigo_equipo_fuera = 10
      ) AS puntos_partidos_empatados_fuera
    USING(codigo_equipo) GROUP BY codigo_equipo ORDER BY PT DESC) AS Puntos
  JOIN (SELECT codigo_equipo,GFC.GFC,GFF.GFF,GCC.GCC,GCF.GCF,(GFC.GFC+GFF.GFF)-(GCC.GCC+GCF.GCF) AS DIF FROM
    (SELECT p.codigo_equipo_casa AS codigo_equipo, SUM(p.resultado_equipo_casa) AS GFC FROM partidos p WHERE p.codigo_competicion=1 GROUP BY p.codigo_equipo_casa) AS GFC
    JOIN (SELECT p.codigo_equipo_fuera AS codigo_equipo, SUM(p.resultado_equipo_fuera) AS GFF FROM partidos p WHERE p.codigo_competicion=1 GROUP BY p.codigo_equipo_fuera) AS GFF
    USING (codigo_equipo)
    JOIN (SELECT p.codigo_equipo_casa AS codigo_equipo, SUM(p.resultado_equipo_fuera) AS GCC FROM partidos p WHERE p.codigo_competicion=1 GROUP BY p.codigo_equipo_casa) AS GCC
    USING (codigo_equipo)
    JOIN (SELECT p.codigo_equipo_fuera AS codigo_equipo, SUM(p.resultado_equipo_casa) AS GCF FROM partidos p WHERE p.codigo_competicion=1 GROUP BY p.codigo_equipo_fuera) AS GCF
    USING (codigo_equipo) GROUP BY codigo_equipo ORDER BY DIF DESC) AS Goles USING (codigo_equipo)
    
    JOIN (SELECT partidos_casa.codigo_equipo, partidos_casa.num_partidos_casa+partidos_fuera.num_partidos_fuera PJ FROM (
    SELECT codigo_equipo_casa codigo_equipo, COUNT(*) num_partidos_casa FROM partidos
    WHERE codigo_competicion=1
    GROUP BY codigo_equipo_casa)partidos_casa INNER JOIN (
    SELECT codigo_equipo_fuera codigo_equipo, COUNT(*) num_partidos_fuera FROM partidos
  WHERE codigo_competicion=1
  GROUP BY codigo_equipo_fuera) partidos_fuera USING (codigo_equipo) GROUP BY codigo_equipo) AS PJ USING (codigo_equipo)) AS Clasificacion JOIN equipos e USING (codigo_equipo) ORDER BY Clasificacion.PT DESC',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        
        return $this->render("vistasclasificacion",[
            "resultados"=>$dataProvider,
            "campos"=>['EQUIPOS','PT','PJ','PPGC','PPEC','PPGF','PPEF','GFC','GFF','GCC','GCF','DIF'],
            "titulo"=>"Clasificacion Liga Santander 19/20",
            "enunciado"=>"",
            
        ]);
    }
    
        public function actionGoleadortotal(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(*) FROM (SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(g.codigo_jugador_gol) AS goles FROM jugadores j INNER JOIN goles g ON j.codigo_jugador = g.codigo_jugador_gol GROUP BY j.codigo_jugador ORDER BY goles DESC) C1')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(g.codigo_jugador_gol) AS goles FROM jugadores j INNER JOIN goles g ON j.codigo_jugador = g.codigo_jugador_gol GROUP BY j.codigo_jugador ORDER BY goles DESC',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        
        return $this->render("vistasgeneralgol",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','apellidos','goles'],
            "titulo"=>"Máximos goleadores entre todas las competiciones",
            "enunciado"=>"",
        ]);
    }
    
            public function actionAsistentetotal(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(*) FROM (SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(g.codigo_jugador_asistencia) AS asistencias FROM jugadores j INNER JOIN goles g ON j.codigo_jugador = g.codigo_jugador_asistencia GROUP BY j.codigo_jugador ORDER BY asistencias DESC)c6')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(g.codigo_jugador_asistencia) AS asistencias FROM jugadores j INNER JOIN goles g ON j.codigo_jugador = g.codigo_jugador_asistencia GROUP BY j.codigo_jugador ORDER BY asistencias DESC',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        
        return $this->render("vistasgeneralasistencia",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','apellidos','asistencias'],
            "titulo"=>"Máximos asistentes entre todas las competiciones",
            "enunciado"=>"",
        ]);
    }
    
                public function actionGoleadorcompeticion($id){
        
        $numero = Yii::$app->db
                ->createCommand("SELECT COUNT(*) FROM (SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(g.codigo_jugador_gol) AS goles FROM jugadores j INNER JOIN goles g ON j.codigo_jugador = g.codigo_jugador_gol INNER JOIN partidos p ON g.codigo_partido = p.codigo_partido WHERE p.codigo_competicion=$id GROUP BY j.codigo_jugador ORDER BY goles DESC) c2")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(g.codigo_jugador_gol) AS goles FROM jugadores j INNER JOIN goles g ON j.codigo_jugador = g.codigo_jugador_gol INNER JOIN partidos p ON g.codigo_partido = p.codigo_partido WHERE p.codigo_competicion=$id GROUP BY j.codigo_jugador ORDER BY goles DESC",
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        $modelo = \app\models\Competiciones::findOne($id);
        $nombre_competicion= \app\models\Competiciones::findOne($id)->nombre;
        $titulo="Máximos goleadores de ".$nombre_competicion;
        
        return $this->render("vistascompeticionesgol",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','apellidos','goles'],
            "titulo"=>"$titulo",
            "enunciado"=>"",
            "codigo"=>$id,
            "modelo"=>$modelo,
        ]);
    }
    
                    public function actionAsistentecompeticion($id){
        
        $numero = Yii::$app->db
                ->createCommand("SELECT COUNT(*) FROM (SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(g.codigo_jugador_asistencia) AS asistencias FROM jugadores j INNER JOIN goles g ON j.codigo_jugador = g.codigo_jugador_asistencia INNER JOIN partidos p ON g.codigo_partido = p.codigo_partido WHERE p.codigo_competicion=$id GROUP BY j.codigo_jugador ORDER BY asistencias DESC)c10")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(g.codigo_jugador_asistencia) AS asistencias FROM jugadores j INNER JOIN goles g ON j.codigo_jugador = g.codigo_jugador_asistencia INNER JOIN partidos p ON g.codigo_partido = p.codigo_partido WHERE p.codigo_competicion=$id GROUP BY j.codigo_jugador ORDER BY asistencias DESC",
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        $modelo = \app\models\Competiciones::findOne($id);
        $nombre_competicion= \app\models\Competiciones::findOne($id)->nombre;
        $titulo="Máximos asistentes de ".$nombre_competicion;
        
        return $this->render("vistascompeticionesasistencia",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','apellidos','asistencias'],
            "titulo"=>$titulo,
            "enunciado"=>"",
            "codigo"=>$id,
            "modelo"=>$modelo,
        ]);
    }
    
                    public function actionGoleadorequipos($id){
        
        $numero = Yii::$app->db
                ->createCommand("SELECT COUNT(*) FROM(SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(g.codigo_jugador_gol) AS goles FROM jugadores j INNER JOIN goles g ON j.codigo_jugador = g.codigo_jugador_gol INNER JOIN forman f ON j.codigo_jugador = f.codigo_jugador WHERE f.codigo_equipo=$id AND f.fecha_baja IS NULL GROUP BY j.codigo_jugador ORDER BY goles DESC) c3")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(g.codigo_jugador_gol) AS goles FROM jugadores j INNER JOIN goles g ON j.codigo_jugador = g.codigo_jugador_gol INNER JOIN forman f ON j.codigo_jugador = f.codigo_jugador WHERE f.codigo_equipo=$id AND f.fecha_baja IS NULL GROUP BY j.codigo_jugador ORDER BY goles DESC",
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        $modelo = Equipos::findOne($id);
        $nombre_equipos= \app\models\Equipos::findOne($id)->nombre_equipo;
        $titulo="Máximos goleadores del ".$nombre_equipos;
        
        return $this->render("vistasequiposgol",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','apellidos','goles'],
            "titulo"=>$titulo,
            "enunciado"=>"",
            "codigo"=>$id,
            "modelo"=>$modelo,
        ]);
    }
    
                    public function actionAsistentesequipos($id){
        
        $numero = Yii::$app->db
                ->createCommand("SELECT * FROM (SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(g.codigo_jugador_asistencia) AS asistencias FROM jugadores j INNER JOIN goles g ON j.codigo_jugador = g.codigo_jugador_asistencia INNER JOIN forman f ON j.codigo_jugador = f.codigo_jugador WHERE f.codigo_equipo=$id AND f.fecha_baja IS NULL GROUP BY j.codigo_jugador ORDER BY asistencias DESC)c11")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(g.codigo_jugador_asistencia) AS asistencias FROM jugadores j INNER JOIN goles g ON j.codigo_jugador = g.codigo_jugador_asistencia INNER JOIN forman f ON j.codigo_jugador = f.codigo_jugador WHERE f.codigo_equipo=$id AND f.fecha_baja IS NULL GROUP BY j.codigo_jugador ORDER BY asistencias DESC",
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        
        $modelo = Equipos::findOne($id);
        $nombre_equipos= \app\models\Equipos::findOne($id)->nombre_equipo;
        $titulo="Máximos asistentes del ".$nombre_equipos;
        
        return $this->render("vistasequiposasistencia",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','apellidos','asistencias'],
            "titulo"=>$titulo,
            "enunciado"=>"",
            "codigo"=>$id,
            "modelo"=>$modelo,
        ]);
    }
  
    
                    public function actionGoleadortotalxpartido(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(*) FROM (SELECT jugados.nombre,jugados.apellidos,goles.goles,jugados.jugados,goles.goles/jugados.jugados AS ratio FROM
  (SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(g.codigo_jugador_gol) AS goles FROM jugadores j INNER JOIN goles g ON j.codigo_jugador = g.codigo_jugador_gol  
 GROUP BY j.codigo_jugador ORDER BY goles DESC) AS goles
  JOIN
(SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(j1.ha_jugado) jugados FROM jugadores j INNER JOIN juegan j1 ON j.codigo_jugador = j1.codigo_jugador  WHERE j1.ha_jugado=1  GROUP BY j.codigo_jugador ORDER BY jugados DESC)
AS jugados
	ON goles.codigo_jugador=jugados.codigo_jugador GROUP BY goles.codigo_jugador ORDER BY goles.goles DESC) as golesjugados')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT jugados.nombre,jugados.apellidos,goles.goles,jugados.jugados,goles.goles/jugados.jugados AS ratio FROM
  (SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(g.codigo_jugador_gol) AS goles FROM jugadores j INNER JOIN goles g ON j.codigo_jugador = g.codigo_jugador_gol  
 GROUP BY j.codigo_jugador ORDER BY goles DESC) AS goles
  JOIN
(SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(j1.ha_jugado) jugados FROM jugadores j INNER JOIN juegan j1 ON j.codigo_jugador = j1.codigo_jugador  WHERE j1.ha_jugado=1  GROUP BY j.codigo_jugador ORDER BY jugados DESC)
AS jugados
	ON goles.codigo_jugador=jugados.codigo_jugador GROUP BY goles.codigo_jugador ORDER BY goles.goles DESC',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        
        return $this->render("vistasgeneralgol",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','apellidos','goles','jugados','ratio'],
            "titulo"=>"Máximos goleadores de entre todas las competiciones por partido jugado",
            "enunciado"=>"",
        ]);
    }
    
                    public function actionAsistentetotalxpartido(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(*) FROM (SELECT jugados.codigo_jugador, jugados.nombre,jugados.apellidos,asistencias.asistencias,jugados.jugados,asistencias.asistencias/jugados.jugados AS ratio FROM
  (SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(g.codigo_jugador_asistencia) AS asistencias FROM jugadores j INNER JOIN goles g ON j.codigo_jugador = g.codigo_jugador_asistencia  
 GROUP BY j.codigo_jugador ORDER BY asistencias DESC) AS asistencias
  JOIN
(SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(j1.ha_jugado) jugados FROM jugadores j INNER JOIN juegan j1 ON j.codigo_jugador = j1.codigo_jugador  WHERE j1.ha_jugado=1  GROUP BY j.codigo_jugador ORDER BY jugados DESC)
AS jugados
	ON asistencias.codigo_jugador=jugados.codigo_jugador GROUP BY asistencias.codigo_jugador ORDER BY asistencias DESC)c7')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT jugados.codigo_jugador, jugados.nombre,jugados.apellidos,asistencias.asistencias,jugados.jugados,asistencias.asistencias/jugados.jugados AS ratio FROM
  (SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(g.codigo_jugador_asistencia) AS asistencias FROM jugadores j INNER JOIN goles g ON j.codigo_jugador = g.codigo_jugador_asistencia  
 GROUP BY j.codigo_jugador ORDER BY asistencias DESC) AS asistencias
  JOIN
(SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(j1.ha_jugado) jugados FROM jugadores j INNER JOIN juegan j1 ON j.codigo_jugador = j1.codigo_jugador  WHERE j1.ha_jugado=1  GROUP BY j.codigo_jugador ORDER BY jugados DESC)
AS jugados
	ON asistencias.codigo_jugador=jugados.codigo_jugador GROUP BY asistencias.codigo_jugador ORDER BY asistencias DESC',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        
        return $this->render("vistasgeneralasistencia",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','apellidos','asistencias','jugados','ratio'],
            "titulo"=>"Máximos asistentes de entre todas las competiciones por partido jugado",
            "enunciado"=>"",
        ]);
    }
    
                    public function actionGoleadorxpartidocompeticion($id){
        
        $numero = Yii::$app->db
                ->createCommand("SELECT * FROM (SELECT jugados.codigo_jugador, jugados.nombre,jugados.apellidos,goles.goles,jugados.jugados,goles.goles/jugados.jugados AS ratio FROM
  (SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(g.codigo_jugador_gol) AS goles FROM jugadores j INNER JOIN goles g ON j.codigo_jugador = g.codigo_jugador_gol INNER JOIN partidos p ON g.codigo_partido = p.codigo_partido 
 WHERE p.codigo_competicion=$id GROUP BY j.codigo_jugador ORDER BY goles DESC) AS goles
  JOIN
(SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(j1.ha_jugado) jugados FROM jugadores j INNER JOIN juegan j1 ON j.codigo_jugador = j1.codigo_jugador INNER JOIN partidos p ON j1.codigo_partido = p.codigo_partido  WHERE j1.ha_jugado=1 AND p.codigo_competicion=$id GROUP BY j.codigo_jugador ORDER BY jugados DESC)
AS jugados
	ON goles.codigo_jugador=jugados.codigo_jugador GROUP BY goles.codigo_jugador ORDER BY goles.goles DESC)c8")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT jugados.codigo_jugador, jugados.nombre,jugados.apellidos,goles.goles,jugados.jugados,goles.goles/jugados.jugados AS ratio FROM
  (SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(g.codigo_jugador_gol) AS goles FROM jugadores j INNER JOIN goles g ON j.codigo_jugador = g.codigo_jugador_gol INNER JOIN partidos p ON g.codigo_partido = p.codigo_partido 
 WHERE p.codigo_competicion=$id GROUP BY j.codigo_jugador ORDER BY goles DESC) AS goles
  JOIN
(SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(j1.ha_jugado) jugados FROM jugadores j INNER JOIN juegan j1 ON j.codigo_jugador = j1.codigo_jugador INNER JOIN partidos p ON j1.codigo_partido = p.codigo_partido  WHERE j1.ha_jugado=1 AND p.codigo_competicion=$id GROUP BY j.codigo_jugador ORDER BY jugados DESC)
AS jugados
	ON goles.codigo_jugador=jugados.codigo_jugador GROUP BY goles.codigo_jugador ORDER BY goles.goles DESC",
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        $modelo = \app\models\Competiciones::findOne($id);
        $nombre_competicion= \app\models\Competiciones::findOne($id)->nombre;
        $titulo="Máximos goleadores de ".$nombre_competicion;
        
        return $this->render("vistascompeticionesgol",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','apellidos','goles','jugados','ratio'],
            "titulo"=>$titulo,
            "enunciado"=>"",
            "codigo"=>$id,
            "modelo"=>$modelo,
        ]);
    }
    
                    public function actionAsistentexpartidocompeticion($id){
        
        $numero = Yii::$app->db
                ->createCommand("SELECT COUNT(*) FROM (SELECT jugados.codigo_jugador, jugados.nombre,jugados.apellidos,asistencias.asistencias,jugados.jugados,asistencias.asistencias/jugados.jugados AS ratio FROM
  (SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(g.codigo_jugador_gol) AS asistencias FROM jugadores j INNER JOIN goles g ON j.codigo_jugador = g.codigo_jugador_asistencia INNER JOIN partidos p ON g.codigo_partido = p.codigo_partido 
 WHERE p.codigo_competicion=$id GROUP BY j.codigo_jugador ORDER BY asistencias DESC) AS asistencias
  JOIN
(SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(j1.ha_jugado) jugados FROM jugadores j INNER JOIN juegan j1 ON j.codigo_jugador = j1.codigo_jugador INNER JOIN partidos p ON j1.codigo_partido = p.codigo_partido  WHERE j1.ha_jugado=1 AND p.codigo_competicion=$id GROUP BY j.codigo_jugador ORDER BY jugados DESC)
AS jugados
	ON asistencias.codigo_jugador=jugados.codigo_jugador GROUP BY asistencias.codigo_jugador ORDER BY asistencias.asistencias DESC)c9")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT jugados.codigo_jugador, jugados.nombre,jugados.apellidos,asistencias.asistencias,jugados.jugados,asistencias.asistencias/jugados.jugados AS ratio FROM
  (SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(g.codigo_jugador_gol) AS asistencias FROM jugadores j INNER JOIN goles g ON j.codigo_jugador = g.codigo_jugador_asistencia INNER JOIN partidos p ON g.codigo_partido = p.codigo_partido 
 WHERE p.codigo_competicion=$id GROUP BY j.codigo_jugador ORDER BY asistencias DESC) AS asistencias
  JOIN
(SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(j1.ha_jugado) jugados FROM jugadores j INNER JOIN juegan j1 ON j.codigo_jugador = j1.codigo_jugador INNER JOIN partidos p ON j1.codigo_partido = p.codigo_partido  WHERE j1.ha_jugado=1 AND p.codigo_competicion=$id GROUP BY j.codigo_jugador ORDER BY jugados DESC)
AS jugados
	ON asistencias.codigo_jugador=jugados.codigo_jugador GROUP BY asistencias.codigo_jugador ORDER BY asistencias.asistencias DESC",
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        
        $modelo = \app\models\Competiciones::findOne($id);
        $nombre_competicion= \app\models\Competiciones::findOne($id)->nombre;
        $titulo="Máximos goleadores de ".$nombre_competicion;
        
        return $this->render("vistascompeticionesasistencia",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','apellidos','asistencias','jugados','ratio'],
            "titulo"=>$titulo,
            "enunciado"=>"",
            "codigo"=>$id,
            "modelo"=>$modelo,
        ]);
    }
    
                        
    public function actionGolespartes(){
        
        $numero = Yii::$app->db
                ->createCommand("SELECT COUNT(*) FROM (SELECT primera_parte.nombre,primera_parte.apellidos,primera_parte.`primera parte`,segunda_parte.`segunda parte` FROM  
  (SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(g.codigo_jugador_gol) AS 'primera parte' 
  FROM jugadores j INNER JOIN goles g ON j.codigo_jugador = g.codigo_jugador_gol  
  WHERE g.minuto<45 
  GROUP BY j.codigo_jugador) AS primera_parte
   JOIN
   (SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(g.codigo_jugador_gol) AS 'segunda parte' 
  FROM jugadores j INNER JOIN goles g ON j.codigo_jugador = g.codigo_jugador_gol  
  WHERE g.minuto>=45
  GROUP BY j.codigo_jugador ) AS segunda_parte
   ON primera_parte.codigo_jugador=segunda_parte.codigo_jugador 
ORDER BY primera_parte.`primera parte`DESC, segunda_parte.`segunda parte` DESC)c10")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT primera_parte.nombre,primera_parte.apellidos,primera_parte.`primera parte`,segunda_parte.`segunda parte` FROM  
  (SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(g.codigo_jugador_gol) AS 'primera parte' 
  FROM jugadores j INNER JOIN goles g ON j.codigo_jugador = g.codigo_jugador_gol  
  WHERE g.minuto<45 
  GROUP BY j.codigo_jugador) AS primera_parte
   JOIN
   (SELECT j.codigo_jugador, j.nombre, j.apellidos, COUNT(g.codigo_jugador_gol) AS 'segunda parte' 
  FROM jugadores j INNER JOIN goles g ON j.codigo_jugador = g.codigo_jugador_gol  
  WHERE g.minuto>=45
  GROUP BY j.codigo_jugador ) AS segunda_parte
   ON primera_parte.codigo_jugador=segunda_parte.codigo_jugador 
ORDER BY primera_parte.`primera parte`DESC, segunda_parte.`segunda parte` DESC",
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        
        return $this->render("vistasgeneralgol",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','apellidos','primera parte','segunda parte'],
            "titulo"=>"Máximos goleadores entre todadas las competiciones dividido en partes",
            "enunciado"=>"",
        ]);
    }
    
                        public function actionGolesxminuto(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(*) FROM (SELECT COUNT(g.minuto) goles, g.minuto FROM jugadores j INNER JOIN goles g ON j.codigo_jugador = g.codigo_jugador_gol GROUP BY g.minuto ORDER BY minuto)c11')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT COUNT(g.minuto) goles, g.minuto FROM jugadores j INNER JOIN goles g ON j.codigo_jugador = g.codigo_jugador_gol GROUP BY g.minuto ORDER BY minuto',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        
        return $this->render("vistasgeneralgol",[
            "resultados"=>$dataProvider,
            "campos"=>['minuto','goles'],
            "titulo"=>"Minutos en los que se producen los goles",
            "enunciado"=>"",
        ]);
    }
    
}
