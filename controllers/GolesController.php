<?php

namespace app\controllers;

use Yii;
use app\models\Goles;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Partidos;
use app\models\Jugadores;
use app\models\Juegan;

/**
 * GolesController implements the CRUD actions for Goles model.
 */
class GolesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Goles models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Goles::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Goles model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Goles model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
public function actionCreate()
    {
        $model = new Goles();
        
        $model_jugadores = Jugadores::find()->all();
         $model_partidos = Yii::$app->db->createCommand('SELECT partidos_fuera.codigo_partido, jornada, partidos_fuera.nombre_equipo_casa, e.nombre_equipo AS nombre_equipo_fuera
   FROM 
  (SELECT partidos1.codigo_partido, partidos1.codigo_equipo_casa,e.nombre_equipo
    nombre_equipo_casa, partidos1.codigo_equipo_fuera,e.nombre_equipo,jornada FROM 
  (SELECT p.codigo_partido, p.codigo_equipo_casa, p.codigo_equipo_fuera,p.jornada FROM partidos p) 
    partidos1 INNER JOIN equipos e ON partidos1.codigo_equipo_casa=e.codigo_equipo) 
partidos_fuera INNER JOIN equipos e ON partidos_fuera.codigo_equipo_fuera=e.codigo_equipo ORDER BY partidos_fuera.codigo_partido')->queryAll();

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
            return $this->redirect(['view', 'id' => $model->codigo_gol]);
        }

        return $this->render('create', [
            'model' => $model,
            'model_jugadores' => $model_jugadores,
            'model_partidos' => $model_partidos,
        ]);
    }

    /**
     * Updates an existing Goles model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model_jugadores = Jugadores::find()->all();
         $model_partidos = Yii::$app->db->createCommand('SELECT partidos_fuera.codigo_partido, jornada, partidos_fuera.nombre_equipo_casa, e.nombre_equipo AS nombre_equipo_fuera
   FROM 
  (SELECT partidos1.codigo_partido, partidos1.codigo_equipo_casa,e.nombre_equipo
    nombre_equipo_casa, partidos1.codigo_equipo_fuera,e.nombre_equipo,jornada FROM 
  (SELECT p.codigo_partido, p.codigo_equipo_casa, p.codigo_equipo_fuera,p.jornada FROM partidos p) 
    partidos1 INNER JOIN equipos e ON partidos1.codigo_equipo_casa=e.codigo_equipo) 
partidos_fuera INNER JOIN equipos e ON partidos_fuera.codigo_equipo_fuera=e.codigo_equipo ORDER BY partidos_fuera.codigo_partido')->queryAll();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo_gol]);
        }

        return $this->render('update', [
            'model' => $model,
            'model_jugadores' => $model_jugadores,
            'model_partidos' => $model_partidos,
        ]);
    }

    /**
     * Deletes an existing Goles model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Goles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Goles the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Goles::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('La página solicitada no existe.');
}

public function actionListjugadores($id)
    {
           $countjugadores = Juegan::find()->where(['codigo_partido'=>$id])->count();
           
           $jugadores = Juegan::find()->where(['codigo_partido'=>$id])
                   ->orderBy('id_juegan DESC')->all();
           
           
           
           if($countjugadores>0){
               echo "<option value='null'>Selecciona en propia puerta o el goleador</option>";
               echo "<option value='null'>En propia puerta</option>";
               foreach($jugadores as $result) echo "<option value='".$result->codigo_jugador."'>".$result->codigoJugador['nombre'].' '.$result->codigoJugador['apellidos']."</option>";
           }
           
           else{
               echo "<option>-</option>";
           }
           
    }
    
    public function actionListjugadores2($id)
    {
           $countjugadores2 = Juegan::find()->where(['codigo_partido'=>$id])->count();
           
           $jugadores2 = Juegan::find()->where(['codigo_partido'=>$id])
                   ->orderBy('id_juegan DESC')->all();
           
           
           
           if($countjugadores2>0){
               echo "<option value='null'>Sin asistencia</option>";
               foreach($jugadores2 as $result) echo "<option value='".$result->codigo_jugador."'>".$result->codigoJugador['nombre'].' '.$result->codigoJugador['apellidos']."</option>";
           }
           
           else{
               echo "<option>-</option>";
           }
           
    }
}
