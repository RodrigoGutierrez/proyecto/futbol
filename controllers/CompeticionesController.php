<?php

namespace app\controllers;

use Yii;
use app\models\Competiciones;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Equipos;
use app\models\Partidos;

/**
 * CompeticionesController implements the CRUD actions for Competiciones model.
 */
class CompeticionesController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Competiciones models.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Competiciones::find(),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndexadmin() {
        $dataProvider = new ActiveDataProvider([
            'query' => Competiciones::find(),
        ]);

        return $this->render('index_admin', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Competiciones model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        $partidos = new ActiveDataProvider([
            'query' => Partidos::find()->where("codigo_competicion=$id"),
            'pagination'=>[
                'pageSize'=>400,
            ],
        ]);

        if (!Yii::$app->user->isGuest) {
            return $this->render('view', [
                        'model' => $this->findModel($id),
                        'partidos' => $partidos,
            ]);
        } else {
            if ($id == 1) {
                return $this->render('vistasliga', [
                            'model' => $this->findModel($id),
                            'partidos' => $partidos,
                ]);
            } else if ($id == 2) {

                return $this->render('vistascopa', [
                            'model' => $this->findModel($id),
                            'partidos' => $partidos,
                ]);
            } else if ($id == 3) {

                return $this->render('vistassupercopa', [
                            'model' => $this->findModel($id),
                            'partidos' => $partidos,
                ]);
            }
        }
    }

    public function actionJornadas($id, $j) {
        $partidos = new ActiveDataProvider([
            'query' => Partidos::find()->where("codigo_competicion=$id AND jornada=$j"),
            'pagination'=>[
                'pageSize'=>400,
            ],
        ]);
        
        if($id==2 && $j==1){
            $partidos = new ActiveDataProvider([
            'query' => Partidos::find()->where("codigo_competicion=$id AND jornada='dieciseisavos'"),
            'pagination'=>[
                'pageSize'=>400,
            ],
        ]);
            }else if($id==2 && $j==2){
            $partidos = new ActiveDataProvider([
            'query' => Partidos::find()->where("codigo_competicion=$id AND jornada='octavos'"),
            'pagination'=>[
                'pageSize'=>400,
            ],
        ]);
            }else if($id==2 && $j==3){
            $partidos = new ActiveDataProvider([
            'query' => Partidos::find()->where("codigo_competicion=$id AND jornada='cuartos'"),
            'pagination'=>[
                'pageSize'=>400,
            ],
        ]);
            }else if($id==2 && $j==4){
            $partidos = new ActiveDataProvider([
            'query' => Partidos::find()->where("codigo_competicion=$id AND jornada='semifinal'"),
            'pagination'=>[
                'pageSize'=>400,
            ],
        ]);
            }else if($id==2 && $j==5){
            $partidos = new ActiveDataProvider([
            'query' => Partidos::find()->where("codigo_competicion=$id AND jornada='final'"),
            'pagination'=>[
                'pageSize'=>400,
            ],
        ]);
            }
            
        if($id==3 && $j==4){
            $partidos = new ActiveDataProvider([
            'query' => Partidos::find()->where("codigo_competicion=$id AND jornada='semifinal'"),
            'pagination'=>[
                'pageSize'=>400,
            ],
        ]);
            }else if($id==3 && $j==5){
            $partidos = new ActiveDataProvider([
            'query' => Partidos::find()->where("codigo_competicion=$id AND jornada='final'"),
            'pagination'=>[
                'pageSize'=>400,
            ],
        ]);
            }    

        if ($id == 1) {
            return $this->render('vistasliga', [
                        'model' => $this->findModel($id),
                        'partidos' => $partidos,
            ]);
        } else if ($id == 2) {

            return $this->render('vistascopa', [
                        'model' => $this->findModel($id),
                        'partidos' => $partidos,
            ]);
        } else if ($id == 3) {

            return $this->render('vistassupercopa', [
                        'model' => $this->findModel($id),
                        'partidos' => $partidos,
            ]);
        }
    }

    /**
     * Creates a new Competiciones model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Competiciones();
        $model_equipos = Equipos::find()->Select("nombre_equipo")->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo_competicion]);
        }

        return $this->render('create', [
                    'model' => $model,
                    'model_equipos' => $model_equipos,
        ]);
    }

    /**
     * Updates an existing Competiciones model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $model_equipos = Equipos::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo_competicion]);
        }

        return $this->render('update', [
                    'model' => $model,
                    'model_equipos' => $model_equipos,
        ]);
    }

    /**
     * Deletes an existing Competiciones model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Competiciones model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Competiciones the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Competiciones::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('La página solicitada no existe.');
    }

}
