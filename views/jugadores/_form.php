<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Jugadores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jugadores-form">
    
    <p class="text-right">*Campos obligatorios</p>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true])->label('Nombre del jugador*') ?>

    <?= $form->field($model, 'apellidos')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dorsal')->textInput() ?>

    <?php

//Calendario
echo $form->field($model, 'fecha_nac')->widget(DatePicker::classname(), [
    'options' => ['placeholder' => 'Enter birth date ...'],
    'pluginOptions' => [
        'autoclose'=>true
    ]
]);
?>
    
        <?php $posicion = [
        'Portero' => 'Portero', 
        'Defensa' => 'Defensa',
        'Centrocampista' => 'Centrocampista',
        'Delantero' => 'Delantero'
        
    ];
?>
    <?= $form->field($model, 'posicion')->dropdownList($posicion)->label('Posición') ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
