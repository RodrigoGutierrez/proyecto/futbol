<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\Equipos */


$this->title = $model->nombre_equipo;
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<?php if (Yii::$app->user->isGuest){ ?>

<div class="cabecera">
  <div class="header1"> </div>
  <div class="cuadrado"> </div>
  <div class="triangulo"> </div>
  <div class="header2"> </div>
</div>

<div class='jumbotron cabecera_escudos'>
    <h2>
        <div><?= Html::a(Html::img('@web/images/'.$model->nombre_equipo.'.png', ['class' => 'img-responsive img-border escudos_vista', 'title' => $model->nombre_equipo])); ?></div>
        <div class="cabecera_equipos2"><?= Html::encode($this->title) ?></div>
    </h2>
    <div class="subcabecera_equipos"><h4>Entrenador: <?= $model->entrenador; ?></h4></div>
    <div class="subcabecera_equipos"><h4>Estadio: <?= $model->estadio; ?></h4></div>
</div>

<div class="container">
    <div class="equipos-view">
    
    

<!--    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->codigo_equipo], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->codigo_equipo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de borrar este registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
            'dorsal',
[
            'attribute' => 'nombre_completo',
            'label' => 'Nombre Completo',
            'value' => function($model) { return $model->nombre  . " " . $model->apellidos ;},
        ],           
            'fecha_nac',
            'posicion',
            
            ['attribute'=>'nacionalidades',
            'value' => function($model) {
                    return join(', ', ArrayHelper::map($model->nacionalidades, 'nombre_nacionalidad', 'nombre_nacionalidad'));
                },
],
]
//            ['class' => 'yii\grid\ActionColumn'],
    ]); ?>

    </div>
</div>
<?php } ?>

<?php if (!Yii::$app->user->isGuest){ ?>
<div class="cabecera">
  <div class="header1"> </div>
  <div class="cuadrado"> </div>
  <div class="triangulo"> </div>
  <div class="header2"> </div>
</div>

<div class="container">
<div class="equipos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->codigo_equipo], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->codigo_equipo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de borrar este registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_equipo',
            'nombre_equipo',
            'entrenador',
            'estadio',
        ],
    ]) ?>

</div>
</div>
<?php } ?>