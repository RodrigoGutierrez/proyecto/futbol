<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Forman */

$this->title = 'Actualizar Forman: ' . $model->id_forman;
$this->params['breadcrumbs'][] = ['label' => 'Forman', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_forman, 'url' => ['view', 'id' => $model->id_forman]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cabecera">
  <div class="header1"> </div>
  <div class="cuadrado"> </div>
  <div class="triangulo"> </div>
  <div class="header2"> </div>
</div>

<div class="container">
<div class="forman-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'model_jugadores' => $model_jugadores,
        'model_equipos' => $model_equipos,
    ]) ?>

</div>
</div>