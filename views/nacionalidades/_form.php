<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Nacionalidades */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nacionalidades-form">

    <?php $form = ActiveForm::begin(); ?>

        
    <?php
    
    $listadejugadores= ArrayHelper::map($model_jugadores, 'codigo_jugador',
            function($model) {
                return $model['nombre'].' '.$model['apellidos'];
            })
    ?>
    
    <?= $form->field($model, 'codigo_jugador')->dropDownList ($listadejugadores)->label('Jugador')?>

    <?= $form->field($model, 'nombre_nacionalidad')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
