<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Nacionalidades */

$this->title = $model->codigoJugador->nombre. ' ' .$model->codigoJugador->apellidos.' nacional de '.$model->nombre_nacionalidad;
$this->params['breadcrumbs'][] = ['label' => 'Nacionalidades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="cabecera">
  <div class="header1"> </div>
  <div class="cuadrado"> </div>
  <div class="triangulo"> </div>
  <div class="header2"> </div>
</div>

<div class="container">
<div class="nacionalidades-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->codigo_nacionalidad], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->codigo_nacionalidad], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de borrar este registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        
        'model' => $model,
        'attributes' => [
            'codigo_nacionalidad',
            [
            'attribute' => 'nombre_completo',
            'label' => 'Nombre Completo',
            'value' => function($model) { return $model->codigoJugador['nombre']  . ' ' . $model->codigoJugador['apellidos'] ;},
        ], 
            'nombre_nacionalidad',
        ],
    ]) ?>

</div>
</div>
