<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Goles */

$this->title = 'Crear Gol';
$this->params['breadcrumbs'][] = ['label' => 'Goles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cabecera">
  <div class="header1"> </div>
  <div class="cuadrado"> </div>
  <div class="triangulo"> </div>
  <div class="header2"> </div>
</div>

<div class="container">
<div class="goles-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'model_jugadores' => $model_jugadores,
        'model_partidos' => $model_partidos,
        
    ]) ?>

</div>
</div>