<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Goles */

if($model->codigoJugadorGol!=null){$this->title = 'Gol de '.$model->codigoJugadorGol['nombre'].' '.$model->codigoJugadorGol['apellidos'].' asistido por '.$model->codigoJugadorAsistencia['nombre'].' '.$model->codigoJugadorAsistencia['apellidos'];
}else{
    $this->title = 'Gol en propia puerta';
}
$this->params['breadcrumbs'][] = ['label' => 'Goles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="cabecera">
  <div class="header1"> </div>
  <div class="cuadrado"> </div>
  <div class="triangulo"> </div>
  <div class="header2"> </div>
</div>

<div class="container">
<div class="goles-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->codigo_gol], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->codigo_gol], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de borrar este registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_gol',
            'codigo_jugador_gol',
            'codigo_jugador_asistencia',
            'codigo_partido',
            'minuto',
        ],
    ]) ?>

</div>
    </div>
