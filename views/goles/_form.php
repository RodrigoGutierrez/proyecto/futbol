<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Partidos;
use app\models\Jugadores;

/* @var $this yii\web\View */
/* @var $model app\models\Goles */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="goles-form">
    <p class="text-right">*Campos obligatorios</p>

    <?php $form = ActiveForm::begin(); ?>

    <?php
    $listadepartidos = ArrayHelper::map($model_partidos, 'codigo_partido', function($model) {
                return $model['nombre_equipo_casa'] . ' - ' . $model['nombre_equipo_fuera'];
            });
    ?>
    <?=
    $form->field($model, 'codigo_partido')->dropDownList($listadepartidos,
            [
                'prompt' => 'Escoge un partido',
                'id' => 'partido',
                'onchange' => '
				$.post( "' . Yii::$app->urlManager->createUrl('goles/listjugadores?id=') . '"+$(this).val(), 
                                    function( data ) {
				  $( "select#jugador" ).html( data );
				});'
    ]);
    ?>
    
    <?php
    $listadejugadores = ArrayHelper::map($model_jugadores, 'codigo_jugador', function($model) {
                return $model['nombre'] . ' ' . $model['apellidos'];
            });
    ?>
    
    <?=
    $form->field($model, 'codigo_jugador_gol')->dropDownList(Jugadores::dropdown(),
            ['id' => 'jugador',
                'prompt' => 'Escoge el jugador que haya marcado el gol',
                'onchange' => '
                                    $.post( "' . Yii::$app->urlManager->createUrl('goles/listjugadores2?id=') . '"+ $("select#partido").val(), function( data ) {
                                      $( "select#jugadorasistente" ).html( data );
                                    });'
    ]);
    ?>
    <?=
    $form->field($model, 'codigo_jugador_asistencia')->dropDownList(Jugadores::dropdown(),
            ['id' => 'jugadorasistente',
                'prompt' => 'Escoge el jugador que ha realizado la asistencia',
    ]);
    ?>
    
    <?= $form->field($model, 'minuto')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
