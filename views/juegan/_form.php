<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Partidos;
use app\models\Jugadores;


/* @var $this yii\web\View */
/* @var $model app\models\Juegan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="juegan-form">

 <?php $form = ActiveForm::begin(); ?>

        <?php 
       $listadepartidos= ArrayHelper::map($model_partidos,'codigo_partido',function($model){
         return $model['jornada'].' - '.$model['nombre_equipo_casa'].' - '.$model['nombre_equipo_fuera'];
     });
    ?>
    
    <?= $form->field($model, 'codigo_partido')->dropDownList($listadepartidos,[
                'prompt' => 'Escoge un partido',
                'id' => 'partido',
                'onchange' => '
                            $.post( "' . Yii::$app->urlManager->createUrl('juegan/listjugadores?id=') . '"+$(this).val(), 
                                    function( data ) {
				  $( "select#jugador" ).html( data );
				});'
    ]); ?>

    
    <?= $form->field($model, 'codigo_jugador')->dropDownList (Jugadores::dropdown(),
            ['id' => 'jugador',
                'prompt' => 'Escoge el jugador que haya jugado el partido',
                
    ]);?>


    <?= $form->field($model, 'ha_jugado')->checkbox(array('label'=>''))
			->label('Participación'); ?>

        <?= $form->field($model, 'lesion')->checkbox(array('label'=>''))
			->label('Se lesionó'); ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
