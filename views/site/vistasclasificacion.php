<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = $titulo;
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="cabecera">
  <div class="header1"> </div>
  <div class="cuadrado"> </div>
  <div class="triangulo"> </div>
  <div class="header2"> </div>
</div>
<div class="jumbotron cabecera_escudos3">
    <?= Html::a(Html::img('@web/images/LaLiga Santander.png', ['class' => 'img-responsive img-border escudos_vista', 'title' => 'LaLiga Santander']), ['competiciones/view','id'=>1]); ?>
    <h2>
        <?= $titulo ?>
    </h2>
    <p class="lead"> <?= $enunciado ?> </p>
</div>

<div class="container bajo_clasificacion">
<?= GridView::widget([
    'dataProvider'=>$resultados,
    'columns'=>[['class' => 'yii\grid\SerialColumn'],
    'EQUIPOS','PT','PJ','PPGC','PPEC','PPGF','PPEF','GFC','GFF','GCC','GCF','DIF'],
    
]); ?>
    <div class="bajo_clasificacion"><b>PT</b>:Puntos totales.&nbsp;&nbsp;&nbsp;<b>Pj</b>:Partidos jugados.&nbsp;&nbsp;&nbsp;<b>Ppgc</b>:Puntos por partido ganado en casa.&nbsp;&nbsp;&nbsp;<b>Ppec</b>:Puntos por partido empatado en casa.&nbsp;&nbsp;&nbsp;<b>Ppgf</b>:Puntos por partido ganado fuera.&nbsp;&nbsp;&nbsp;<b>Ppef</b>:Puntos por partido empatado fuera.&nbsp;&nbsp;&nbsp;<b>Gcf</b>:Goles en contra fuera.&nbsp;&nbsp;&nbsp;<b>Gff</b>:Goles a favor fuera.&nbsp;&nbsp;&nbsp;<b>Gcc</b>:Goles en contra en casa.&nbsp;&nbsp;&nbsp;<b>Gcf</b>:Goles a favor en casa.&nbsp;&nbsp;&nbsp;<b>Dif</b>:Diferencia de goles.&nbsp;&nbsp;&nbsp;</div>
</div>