<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use yii\helpers\Html;
use yii\grid\GridView;
$this->title = $titulo;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cabecera">
  <div class="header1"> </div>
  <div class="cuadrado"> </div>
  <div class="triangulo"> </div>
  <div class="header2"> </div>
</div>
<div class="jumbotron cabecera_escudos2">
    <div class='cabecera_escudos'><?= Html::a(Html::img('@web/images/'.$modelo->nombre.'.png', ['class' => 'img-responsive img-border escudos_vista', 'title' => $modelo->nombre])); ?></div>
    <h2>
        <?= $titulo ?>
    </h2>
    
    <div class="btn-group" role="group" aria-label="...">
    <?= \yii\helpers\Html::a('Volver',Yii::$app->request->referrer,['class' => 'btn btn-success']);?>
    <div class="btn-group">
  <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
     Competiciones<span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
    <li><?= Html::a('LaLiga Santander', ['site/goleadorcompeticion','id'=>1], ['class' => 'btn btn-success']) ?></a></li>
    <li><?= Html::a('Copa del Rey', ['site/goleadorcompeticion','id'=>2], ['class' => 'btn btn-success']) ?></a></li>
    <li><?= Html::a('Supercopa de España', ['site/goleadorcompeticion','id'=>3], ['class' => 'btn btn-success']) ?></a></li>
  </ul>
     <?= Html::a('Por partido jugado', ['site/goleadorxpartidocompeticion','id'=>$codigo], ['class' => 'btn btn-success']) ?>
</div>
</div>
</div>

<div class="container">
<?= GridView::widget([
    'dataProvider'=>$resultados,
    'columns'=>$campos,
    
]); ?>
</div>