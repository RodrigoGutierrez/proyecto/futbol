<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = $titulo;
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="cabecera">
  <div class="header1"> </div>
  <div class="cuadrado"> </div>
  <div class="triangulo"> </div>
  <div class="header2"> </div>
</div>
<div class="jumbotron cabecera_escudos2">
     <div class='cabecera_escudos'><?= Html::a(Html::img('@web/images/'.$modelo->nombre_equipo.'.png', ['class' => 'img-responsive img-border escudos_vista', 'title' => $modelo->nombre_equipo])); ?></div>
    <h2>
        <?= $titulo ?>
    </h2>
    <p class="lead"> <?= $enunciado ?> </p>
    <div class="btn-group">
  <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
     Equipos<span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
    <li><?= Html::a('Athletic Club', ['site/asistentesequipos','id'=>1], ['class' => 'btn btn-success']) ?></a></li>
    <li><?= Html::a('Getafe Club de Fútbol', ['site/asistentesequipos','id'=>2], ['class' => 'btn btn-success']) ?></a></li>
    <li><?= Html::a('Real Club Celta de Vigo', ['site/asistentesequipos','id'=>3], ['class' => 'btn btn-success']) ?></a></li>
    <li><?= Html::a('Sevilla Fútbol Club', ['site/asistentesequipos','id'=>4], ['class' => 'btn btn-success']) ?></a></li>
    <li><?= Html::a('Club Atlético de Madrid', ['site/asistentesequipos','id'=>5], ['class' => 'btn btn-success']) ?></a></li>
    <li><?= Html::a('Club Atlético Osasuna', ['site/asistentesequipos','id'=>6], ['class' => 'btn btn-success']) ?></a></li>
    <li><?= Html::a('Real Club Deportivo Español', ['site/asistentesequipos','id'=>7], ['class' => 'btn btn-success']) ?></a></li>
    <li><?= Html::a('Sociedad Deportiva Eibar', ['site/asistentesequipos','id'=>8], ['class' => 'btn btn-success']) ?></a></li>
    <li><?= Html::a('Club Deportivo Leganés', ['site/asistentesequipos','id'=>9], ['class' => 'btn btn-success']) ?></a></li>
    <li><?= Html::a('Levante Unión Deportiva', ['site/asistentesequipos','id'=>10], ['class' => 'btn btn-success']) ?></a></li>
    <li><?= Html::a('Real Madrid Club de Fútbol', ['site/asistentesequipos','id'=>11], ['class' => 'btn btn-success']) ?></a></li>
    <li><?= Html::a('Granada Club de Fútbol', ['site/asistentesequipos','id'=>12], ['class' => 'btn btn-success']) ?></a></li>
    <li><?= Html::a('Deportivo Alavés', ['site/asistentesequipos','id'=>13], ['class' => 'btn btn-success']) ?></a></li>
    <li><?= Html::a('Real Club Deportivo Mallorca', ['site/asistentesequipos','id'=>14], ['class' => 'btn btn-success']) ?></a></li>
    <li><?= Html::a('Real Sociedad de Fútbol', ['site/asistentesequipos','id'=>15], ['class' => 'btn btn-success']) ?></a></li>
    <li><?= Html::a('Valencia Club de Fútbol', ['site/asistentesequipos','id'=>16], ['class' => 'btn btn-success']) ?></a></li>
    <li><?= Html::a('Fútbol Club Barcelona', ['site/asistentesequipos','id'=>17], ['class' => 'btn btn-success']) ?></a></li>
    <li><?= Html::a('Real Betis Balompié', ['site/asistentesequipos','id'=>18], ['class' => 'btn btn-success']) ?></a></li>
    <li><?= Html::a('Real Valladolid Club de Fútbol', ['site/asistentesequipos','id'=>19], ['class' => 'btn btn-success']) ?></a></li>
    <li><?= Html::a('Villarreal Club de Fútbol', ['site/asistentesequipos','id'=>20], ['class' => 'btn btn-success']) ?></a></li>
  </ul>
</div>
</div>

<div class="container" style="min-height: 900px;">
<?= GridView::widget([
    'dataProvider'=>$resultados,
    'columns'=>$campos,
    
]); ?>
</div>