<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = $titulo;
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="cabecera">
  <div class="header1"> </div>
  <div class="cuadrado"> </div>
  <div class="triangulo"> </div>
  <div class="header2"> </div>
</div>
<div class="jumbotron cabecera_escudos2">
    <h2>
        <?= $titulo ?>
    </h2>   
    <div class="btn-group" role="group" aria-label="...">
    <?= \yii\helpers\Html::a('Volver',Yii::$app->request->referrer,['class' => 'btn btn-success']);?>
    <?= Html::a('Por partido jugado', ['site/goleadortotalxpartido'], ['class' => 'btn btn-success']) ?>
    <?= Html::a('Por partes', ['site/golespartes'], ['class' => 'btn btn-success']) ?>
    <?= Html::a('Por minutos', ['site/golesxminuto'], ['class' => 'btn btn-success']) ?>
    </div>
</div>

<div class="container">
<?= GridView::widget([
    'dataProvider'=>$resultados,
    'columns'=>$campos,
    
]); ?>
</div>

