<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
$this->params['fluid'] = true;
$this->title = 'Estadisticas de futbol';

?>
<!--inicio del header-->
<div class="cabecera">
  <div class="header1"> </div>
  <div class="cuadrado"> </div>
  <div class="triangulo"> </div>
  <div class="header2"> </div>
</div>

    <div class= "jumbotron text-center" >
        <?= Html::a(Html::img('@web/images/Deportivo Alavés.png', ['class' => 'img-responsive img-border escudo escudos_pequeños', 'title' => 'Deportivo Alavés']), ['equipos/view','id'=>13]); ?>
        <?= Html::a(Html::img('@web/images/Athletic Club.png', ['class' => 'img-responsive img-border escudo', 'title' => 'Athletic Club']), ['equipos/view','id'=>1]); ?>
        <?= Html::a(Html::img('@web/images/Club Atlético de Madrid.png', ['class' => 'img-responsive img-border escudo', 'title' => 'Club Atlético de Madrid']), ['equipos/view','id'=>5]); ?>
        <?= Html::a(Html::img('@web/images/Fútbol Club Barcelona.png', ['class' => 'img-responsive img-border escudo', 'title' => 'Fútbol Club Barcelona']), ['equipos/view','id'=>17]); ?>
        <?= Html::a(Html::img('@web/images/Real Betis Balompié.png', ['class' => 'img-responsive img-border escudo escudos_pequeños', 'title' => 'Real Betis Balompié']), ['equipos/view','id'=>18]); ?>
        <?= Html::a(Html::img('@web/images/Real Club Celta de Vigo.png', ['class' => 'img-responsive img-border escudo', 'title' => 'Real Club Celta de Vigo']), ['equipos/view','id'=>3]); ?>
        <?= Html::a(Html::img('@web/images/Sociedad Deportiva Eibar.png', ['class' => 'img-responsive img-border escudo', 'title' => 'Sociedad Deportiva Eibar']), ['equipos/view','id'=>8]); ?>
        <?= Html::a(Html::img('@web/images/Real Club Deportivo Español.png', ['class' => 'img-responsive img-border escudo', 'title' => 'Real Club Deportivo Español']), ['equipos/view','id'=>7]); ?>
        <?= Html::a(Html::img('@web/images/Getafe Club de Fútbol.png', ['class' => 'img-responsive img-border escudo', 'title' => 'Getafe Club de Fútbol']), ['equipos/view','id'=>2]); ?>
        <?= Html::a(Html::img('@web/images/Granada Club de Fútbol.png', ['class' => 'img-responsive img-border escudo escudos_pequeños', 'title' => 'Granada Club de Fútbol']), ['equipos/view','id'=>12]); ?>
        <?= Html::a(Html::img('@web/images/Club Deportivo Leganés.png', ['class' => 'img-responsive img-border escudo', 'title' => 'Club Deportivo Leganés']), ['equipos/view','id'=>9]); ?>
        <?= Html::a(Html::img('@web/images/Levante Unión Deportiva.png', ['class' => 'img-responsive img-border escudo levante', 'title' => 'Levante Unión Deportiva']), ['equipos/view','id'=>10]); ?>
        <?= Html::a(Html::img('@web/images/Real Club Deportivo Mallorca.png', ['class' => 'img-responsive img-border escudo', 'title' => 'Real Club Deportivo Mallorca']), ['equipos/view','id'=>14]); ?>
        <?= Html::a(Html::img('@web/images/Club Atlético Osasuna.png', ['class' => 'img-responsive img-border escudo', 'title' => 'Club Atlético Osasuna']), ['equipos/view','id'=>6]); ?>
        <?= Html::a(Html::img('@web/images/Real Sociedad de Fútbol.png', ['class' => 'img-responsive img-border escudo', 'title' => 'Real Sociedad de Fútbol']), ['equipos/view','id'=>15]); ?>
        <?= Html::a(Html::img('@web/images/Real Madrid Club de Fútbol.png', ['class' => 'img-responsive img-border escudo', 'title' => 'Real Madrid Club de Fútbol']), ['equipos/view','id'=>11]); ?>
        <?= Html::a(Html::img('@web/images/Real Valladolid Club de Fútbol.png', ['class' => 'img-responsive img-border escudo', 'title' => 'Real Valladolid Club de Fútbol']), ['equipos/view','id'=>19]); ?>
        <?= Html::a(Html::img('@web/images/Sevilla Fútbol Club.png', ['class' => 'img-responsive img-border escudo', 'title' => 'Sevilla Fútbol Club']), ['equipos/view','id'=>4]); ?>
        <?= Html::a(Html::img('@web/images/Valencia Club de Fútbol.png', ['class' => 'img-responsive img-border escudo', 'title' => 'Valencia Club de Fútbol']), ['equipos/view','id'=>16]); ?>
        <?= Html::a(Html::img('@web/images/Villarreal Club de Fútbol.png', ['class' => 'img-responsive img-border escudo', 'title' => 'Villarreal Club de Fútbol']), ['equipos/view','id'=>20]); ?>
    </div>

<div class="container<?= $this->params['fluid'] ? '-fluid' : '' ?> "> 
    <div class="container">
        <div class="text-center">
            
            <div class="container-fluid">
                <div class="row">
                    <div class="col-center">
                        <div class="thumbnail">
                            <div class="text-right alineacion"><h5></h5></div>
                                <div class="caption">
                                    <p><h2 class='h2header'>COMPETICIONES</h2></p>                           
                                    <div class="container-fluid">                                   
                                        <div class="row">
                        
                                            <div class="col-md-4">
                                                <div class="thumbnail thumb">
                                                    <div class="caption">
                                                        <h4>COPA DEL REY</h4>
                                                        <?= Html::a('Ver partidos', ['competiciones/view','id'=>2], ['class' => 'btn btn-success']) ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="thumbnail thumb">
                                                    <div class="caption">
                                                        <h4>LA LIGA SANTANDER</h4>
                                                        <?= Html::a('Ver partidos', ['competiciones/view','id'=>1], ['class' => 'btn btn-success']) ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="thumbnail thumb">
                                                    <div class="caption">
                                                        <h4>SUPERCOPA DE ESPAÑA</h4>
                                                        <?= Html::a('Ver partidos', ['competiciones/view','id'=>3], ['class' => 'btn btn-success']) ?>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                    </div>  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="container-fluid">
                <div class="row">
                    <div class="col-center">
                        <div class="thumbnail">
                            <div class="text-right alineacion"><h5></h5></div> 
                                <div class="caption">
                                    <p><h2 class='h2header'>ESTADÍSTICAS</h2></p>                           
                                    <div class="container-fluid">                                   
                                        <div class="row">
                        
                                        <div class="col-md-4">
                                            <div class="thumbnail thumb">
                                                <div class="caption">
                                                    <h4>EQUIPOS</h4>
                                                    <?= Html::a('Goles', ['site/goleadorequipos','id'=>1], ['class' => 'btn btn-success']) ?>
                                                    <?= Html::a('Asistencias', ['site/asistentesequipos','id'=>1], ['class' => 'btn btn-success']) ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="thumbnail thumb">
                                                <div class="caption">
                                                    <h4>GENERALES</h4>
                                                    <?= Html::a('Goles', ['site/goleadortotal'], ['class' => 'btn btn-success']) ?>
                                                    <?= Html::a('Asistencias', ['site/asistentetotal'], ['class' => 'btn btn-success']) ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="thumbnail thumb">
                                                <div class="caption">
                                                    <h4>COMPETICIONES</h4>
                                                    <?= Html::a('Goles', ['site/goleadorcompeticion','id'=>1], ['class' => 'btn btn-success']) ?>
                                                    <?= Html::a('Asistencias', ['site/asistentecompeticion','id'=>1], ['class' => 'btn btn-success']) ?>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="container-fluid">
                <div class="row">
                    <div class="col-center">
                        <div class="thumbnail">
                            <div class="text-right alineacion"><h5></h5></div> 
                                <div class="caption">
                                    <p><h2 class='h2header'>DATOS</h2></p>                           
                                    <div class="container-fluid">                                   
                                        <div class="row">
                
                                        <div class="col-md-6">
                                            <div class="thumbnail thumb">
                                                <div class="caption">
                                                    <h2>CLASIFICACIÓN</h2>
                                                    <p>de La Liga Santander</p>
                                                    <p><?= Html::a('Ver Clasificacion', ['site/clasificacion'], ['class' => 'btn btn-success']) ?></p> 
                                                </div>
                                            </div>
                                        </div>    
                                        <div class="col-md-6">
                                            <div class="thumbnail thumb">
                                                <div class="caption">
                                                    <h2>CALENDARIO</h2>
                                                    <p>de La Liga Santander</p>
                                                    <p><?= Html::a('Ver partidos', ['competiciones/view','id'=>1], ['class' => 'btn btn-success']) ?></p> 
                                                </div>
                                            </div>
                                        </div>
                                            
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>