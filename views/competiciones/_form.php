<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use kartikorm\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Competiciones */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="competiciones-form">
    
    <p class="text-right">*Campos obligatorios</p>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true])->label('Nombre de la competición*') ?>
            
    <?php 
      $ganador= ArrayHelper::map($model_equipos,'nombre_equipo','nombre_equipo');
      $ganador['Por determinar'] = 'Por determinar'; 
    ?> 
    
    <?= $form->field($model, 'ganador')->dropDownList($ganador)->label('Ganador')?>
    
    <?php $tipo = [
        'Liga' => 'Liga', 
        'Copa' => 'Copa',
        'Super Copa' => 'Super Copa'
        
    ];
?>
    <?= $form->field($model, 'tipo')->dropdownList($tipo)->label('Tipo') ?>

    <?= $form->field($model, 'num_equipos')->textInput()->label('Número de equipos') ?>
    

<?php

//Calendario
echo $form->field($model, 'anio_inicio')->widget(DatePicker::classname(), [
    'options' => ['placeholder' => 'Enter birth date ...'],
    'pluginOptions' => [
        'autoclose'=>true
    ]
]);
?>

    <?php

//Calendario
echo $form->field($model, 'anio_fin')->widget(DatePicker::classname(), [
    'options' => ['placeholder' => 'Enter birth date ...'],
    'pluginOptions' => [
        'autoclose'=>true
    ]
]);
?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
