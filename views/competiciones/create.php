<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Competiciones */

$this->title = 'Crear Competicion';
$this->params['breadcrumbs'][] = ['label' => 'Competiciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cabecera">
  <div class="header1"> </div>
  <div class="cuadrado"> </div>
  <div class="triangulo"> </div>
  <div class="header2"> </div>
</div>

<div class="container">
<div class="competiciones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'model_equipos' => $model_equipos,
    ]) ?>

</div>
</div>