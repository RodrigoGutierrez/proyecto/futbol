<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\Equipos */



\yii\web\YiiAsset::register($this);
?>

<div class="col-md-3 col-md-push-1 jornadas">
    <div class="ajuste_altura_jornadas">
        <div class="jornadas_cabecera">
            <h4>Jornada: <?= $model->jornada ?></h4>
        </div>
        <h2><?= $model->codigoEquipoCasa->nombre_equipo?>-<?= $model->codigoEquipoFuera->nombre_equipo?></h2>
        <h2>Resultado: <?= $model['resultado_equipo_casa']?>-<?= $model['resultado_equipo_fuera']?></h2>
        <h2>Estadio: <?= $model->estadio ?></h2>
        <hr>
        <h4>Goles en los minutos: <div style="display:inline;" ><?= implode('</div><div style="display:inline-block !important;margin-left:5px;">',ArrayHelper::getColumn($model->goles, 'minuto')) ?></div> </h4>
        <hr>
           
    </div>      
</div>
    