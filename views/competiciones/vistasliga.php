<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model app\models\Competiciones */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<?php if (Yii::$app->user->isGuest){ ?>

<div class="cabecera">
  <div class="header1"> </div>
  <div class="cuadrado"> </div>
  <div class="triangulo"> </div>
  <div class="header2"> </div>
</div>

<div class='jumbotron cabecera_escudos'>
    <h2>
        <div><?= Html::a(Html::img('@web/images/'.$model->nombre.'.png', ['class' => 'img-responsive img-border escudos_vista', 'title' => $model->nombre])); ?></div>
        <div class="cabecera_equipos2"><?= Html::encode($this->title) ?></div>
    </h2>
    
    <div class="subcabecera_equipos"><h4>Numero de equipos: <?= $model->num_equipos; ?></h4></div>
    <div class="subcabecera_equipos"><h4>Fecha de inicio: <?= $model->anio_inicio; ?></h4></div>
    <div class="subcabecera_equipos"><h4>Fecha de fin: <?= $model->anio_fin; ?></h4></div>
    
    <div class="btn-group">
  <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
     Jornada<span class="caret"></span> 
  </button>
  <ul class="dropdown-menu">
    <li><?= Html::a('1', ['competiciones/jornadas', 'id' => 1, 'j'=>1], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('2', ['competiciones/jornadas', 'id' => 1, 'j'=>2], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('3', ['competiciones/jornadas', 'id' => 1, 'j'=>3], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('4', ['competiciones/jornadas', 'id' => 1, 'j'=>4], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('5', ['competiciones/jornadas', 'id' => 1, 'j'=>5], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('6', ['competiciones/jornadas', 'id' => 1, 'j'=>6], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('7', ['competiciones/jornadas', 'id' => 1, 'j'=>7], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('8', ['competiciones/jornadas', 'id' => 1, 'j'=>8], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('9', ['competiciones/jornadas', 'id' => 1, 'j'=>9], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('10', ['competiciones/jornadas', 'id' => 1, 'j'=>10], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('11', ['competiciones/jornadas', 'id' => 1, 'j'=>11], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('12', ['competiciones/jornadas', 'id' => 1, 'j'=>12], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('13', ['competiciones/jornadas', 'id' => 1, 'j'=>13], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('14', ['competiciones/jornadas', 'id' => 1, 'j'=>14], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('15', ['competiciones/jornadas', 'id' => 1, 'j'=>15], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('16', ['competiciones/jornadas', 'id' => 1, 'j'=>16], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('17', ['competiciones/jornadas', 'id' => 1, 'j'=>17], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('18', ['competiciones/jornadas', 'id' => 1, 'j'=>18], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('19', ['competiciones/jornadas', 'id' => 1, 'j'=>19], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('20', ['competiciones/jornadas', 'id' => 1, 'j'=>20], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('21', ['competiciones/jornadas', 'id' => 1, 'j'=>21], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('22', ['competiciones/jornadas', 'id' => 1, 'j'=>22], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('23', ['competiciones/jornadas', 'id' => 1, 'j'=>23], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('24', ['competiciones/jornadas', 'id' => 1, 'j'=>24], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('25', ['competiciones/jornadas', 'id' => 1, 'j'=>25], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('26', ['competiciones/jornadas', 'id' => 1, 'j'=>26], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('27', ['competiciones/jornadas', 'id' => 1, 'j'=>27], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('28', ['competiciones/jornadas', 'id' => 1, 'j'=>28], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('29', ['competiciones/jornadas', 'id' => 1, 'j'=>29], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('30', ['competiciones/jornadas', 'id' => 1, 'j'=>30], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('31', ['competiciones/jornadas', 'id' => 1, 'j'=>31], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('32', ['competiciones/jornadas', 'id' => 1, 'j'=>32], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('33', ['competiciones/jornadas', 'id' => 1, 'j'=>33], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('34', ['competiciones/jornadas', 'id' => 1, 'j'=>34], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('35', ['competiciones/jornadas', 'id' => 1, 'j'=>35], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('36', ['competiciones/jornadas', 'id' => 1, 'j'=>36], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('37', ['competiciones/jornadas', 'id' => 1, 'j'=>37], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('38', ['competiciones/jornadas', 'id' => 1, 'j'=>38], ['class' => 'profile-link']) ?></li>
  </ul>
</div>
    
</div>

<div class="container">
<div class="competiciones-view">



<!--    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->codigo_competicion], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->codigo_competicion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>-->

    <div class="container">    
<?= ListView::widget([
        'dataProvider' =>  $partidos,
        'itemView' =>  '_view',
        
 
    ]); ?>
     
</div>

</div>
</div>
<?php } ?>

<?php if (!Yii::$app->user->isGuest){ ?>
<div class="cabecera">
  <div class="header1"> </div>
  <div class="cuadrado"> </div>
  <div class="triangulo"> </div>
  <div class="header2"> </div>
</div>

<div class="container">
<div class="competiciones-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->codigo_competicion], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->codigo_competicion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_competicion',
            'nombre',
            'ganador',
            'tipo',
            'num_equipos',
            'anio_inicio',
            'anio_fin',
        ],
    ]) ?>

</div>
</div>
<?php } ?>
