<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model app\models\Competiciones */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<?php if (Yii::$app->user->isGuest){ ?>

<div class="cabecera">
  <div class="header1"> </div>
  <div class="cuadrado"> </div>
  <div class="triangulo"> </div>
  <div class="header2"> </div>
</div>

<div class='jumbotron cabecera_escudos'>
    <h2>
        <div><?= Html::a(Html::img('@web/images/'.$model->nombre.'.png', ['class' => 'img-responsive img-border escudos_vista', 'title' => $model->nombre])); ?></div>
        <div class="cabecera_equipos2"><?= Html::encode($this->title) ?></div>
    </h2>
    
    <div class="subcabecera_equipos"><h4>Numero de equipos: <?= $model->num_equipos; ?></h4></div>
    <div class="subcabecera_equipos"><h4>Fecha de inicio: <?= $model->anio_inicio; ?></h4></div>
    <div class="subcabecera_equipos"><h4>Fecha de fin: <?= $model->anio_fin; ?></h4></div>
    
    <div class="btn-group">
  <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
     Jornada<span class="caret"></span> 
  </button>
  <ul class="dropdown-menu">
    <li><?= Html::a('semifinal', ['competiciones/jornadas', 'id' => 3, 'j'=>4], ['class' => 'profile-link']) ?></li>
    <li><?= Html::a('final', ['competiciones/jornadas', 'id' => 3, 'j'=>5], ['class' => 'profile-link']) ?></li>
  </ul>
</div>
    
</div>

<div class="container">
<div class="competiciones-view">



<!--    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->codigo_competicion], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->codigo_competicion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>-->

    <div class="container">    
<?= ListView::widget([
        'dataProvider' =>  $partidos,
        'itemView' =>  '_view',
        
 
    ]); ?>
     
</div>

</div>
</div>
<?php } ?>

<?php if (!Yii::$app->user->isGuest){ ?>
<div class="cabecera">
  <div class="header1"> </div>
  <div class="cuadrado"> </div>
  <div class="triangulo"> </div>
  <div class="header2"> </div>
</div>

<div class="container">
<div class="competiciones-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->codigo_competicion], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->codigo_competicion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_competicion',
            'nombre',
            'ganador',
            'tipo',
            'num_equipos',
            'anio_inicio',
            'anio_fin',
        ],
    ]) ?>

</div>
</div>
<?php } ?>
